import { NationalTreasuresNg2Page } from './app.po';

describe('national-treasures-ng2 App', () => {
  let page: NationalTreasuresNg2Page;

  beforeEach(() => {
    page = new NationalTreasuresNg2Page();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!!');
  });
});

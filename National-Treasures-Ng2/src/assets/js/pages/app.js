﻿var owl = $('.sec-carousel .owl-carousel');
var owl2 = $('.sec-carousel2 .owl-carousel');
$(function () {
    owl.owlCarousel({
        //animateIn: 'fadeIn',
        //animateOut: 'fadeOut',
        margin: 17,
        loop: true,
        items: 1,
        autoWith: true,
        nav: false,
        dots: true
    });
    // Listen to owl events:
    owl.on('changed.owl.carousel', function (event) {

    });
});

$(function () {
    owl2.owlCarousel({
        //animateIn: 'fadeIn',
        //animateOut: 'fadeOut',
        margin: 17,
        loop: true,
        items: 1,
        autoWith: true,
        nav: false,
        dots: true
    });
    // Listen to owl events:
    owl2.on('changed.owl.carousel', function (event) {

    });
});


$(document).on('click', '.sec-carousel .ctrl-slide .btn-prev', function () {
    owl.trigger('prev.owl.carousel');
});

$(document).on('click', '.sec-carousel .ctrl-slide .btn-next', function () {
    owl.trigger('next.owl.carousel');
});


$(document).on('click', '.sec-carousel2 .ctrl-slide .btn-prev', function () {
    //set source
    owl2.trigger('prev.owl.carousel');
});

$(document).on('click', '.sec-carousel2 .ctrl-slide .btn-next', function () {
    //set source
    owl2.trigger('next.owl.carousel');
});

﻿$("#form-new-business").validate({
    submitHandler: function (form) {
        // some other code
        // maybe disabling submit button
        // then:
        $(form).submit();
    }
});

$(function () {

});

var app = angular.module('myApp', []);
app.controller('myPage', function ($scope, $http, $timeout) {
    function readURL(input) {

        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $(input).parent().css({ 'background-image': 'url(' + e.target.result + ')', 'background-size': 'cover' });
                $(input).css({ 'opacity': '0' });
            }

            reader.readAsDataURL(input.files[0]);
        }
    }

    $(".custom-file-input").change(function () {
        readURL(this);
    });

});
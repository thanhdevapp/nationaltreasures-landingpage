import { Routes, RouterModule } from '@angular/router';
import { LandingAppComponent } from './landing-app.component';

const routes: Routes = [
  { path: '', component: LandingAppComponent } 
];

export const LandingAppRoutes = RouterModule.forChild(routes);

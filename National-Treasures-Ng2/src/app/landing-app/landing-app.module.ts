import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LandingAppComponent } from './landing-app.component';
import { LandingAppRoutes } from './landing-app.routing';
import { ContactUsComponent } from './contact-us/contact-us.component';
import { ReactiveFormsModule } from '@angular/forms';

@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,
    LandingAppRoutes
  ],
  declarations: [
    LandingAppComponent,
    ContactUsComponent
]
})
export class LandingAppModule { }
import { Component, OnInit, ViewEncapsulation } from '@angular/core';

declare var $;

@Component({
  selector: 'app-sign-up-school-fail',
  templateUrl: './sign-up-school-fail.component.html',
  styleUrls: ['../sign-up-school-success/sign-up-school-success.component.less'],
  encapsulation: ViewEncapsulation.None,
})
export class SignUpSchoolFailComponent implements OnInit {

  constructor() { }

  ngOnInit() {
    $('.nano').nanoScroller();
  }

}

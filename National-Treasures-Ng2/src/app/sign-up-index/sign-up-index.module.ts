import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SignUpIndexComponent } from './sign-up-index.component';
import { SignUpIndexRoutes } from './sign-up-index.routing';

@NgModule({
  imports: [
    CommonModule,
    SignUpIndexRoutes
  ],
  declarations: [SignUpIndexComponent]
})
export class SignUpIndexModule { }
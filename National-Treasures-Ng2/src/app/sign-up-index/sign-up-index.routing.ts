import { Routes, RouterModule } from '@angular/router';
import { SignUpIndexComponent } from './sign-up-index.component';

const routes: Routes = [
  { path: '', component: SignUpIndexComponent } 
];

export const SignUpIndexRoutes = RouterModule.forChild(routes);

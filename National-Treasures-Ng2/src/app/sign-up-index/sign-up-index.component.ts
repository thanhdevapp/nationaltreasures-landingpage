import { Component, OnInit, ViewEncapsulation } from '@angular/core';

declare var $;

@Component({
  selector: 'app-sign-up-index',
  templateUrl: './sign-up-index.component.html',
  styleUrls: ['./sign-up-index.component.less'],
  encapsulation: ViewEncapsulation.None 
})
export class SignUpIndexComponent implements OnInit {

  constructor() { }

  ngOnInit() {
    $('.nano').nanoScroller();
  }

}

import { Component, OnInit, ViewEncapsulation } from '@angular/core';

declare var $;
@Component({
  selector: 'app-sign-up-school-success',
  templateUrl: './sign-up-school-success.component.html',
  styleUrls: ['./sign-up-school-success.component.less'],
  encapsulation: ViewEncapsulation.None,
})
export class SignUpSchoolSuccessComponent implements OnInit {

  constructor() { }

  ngOnInit() {
    $('.nano').nanoScroller();
  }

}

import { Component, OnInit, ViewEncapsulation } from '@angular/core';

declare var $;
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.less'],
  encapsulation: ViewEncapsulation.None
})
export class AppComponent implements OnInit {
  ngOnInit(): void {    
    $(document).ready(function () {      
      $('body').append('<div class="error-fixed-bottom"><img class="icon pull-left" src="/assets/icons/ic-submit-successful.svg" /><span class="msg"></span><a class="btn btn-dismiss pull-right">Close</a></div>');
      $('.error-fixed-bottom').delay(600).queue(function (next) {
        $(this).css({ 'transform': 'scale(0)' });
        next();
      });
    });
    $(document).on('click', '.error-fixed-bottom .btn-dismiss', function () {
      $('.error-fixed-bottom').css({ 'opacity': 0 });
      $('.error-fixed-bottom').delay(600).queue(function (next) {
        $(this).css({ 'transform': 'scale(0)' });
        next();
      });
    });     
  }
}

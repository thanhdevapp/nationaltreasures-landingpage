import { Injectable } from '@angular/core';
import { HttpService } from '../shared/services/http.service';

@Injectable()
export class SignUpBusinessService {

constructor(private http: HttpService) { }

    async uploadLogo(form: any)
    {
        let url = 'http://ec2-54-89-34-55.compute-1.amazonaws.com:20200/api/storages/logos/upload';
        return await this.http.uploadApiAsync<any[]>(url,form);
    }

    async createBusinees(data: any)
    {
        let url = 'http://ec2-54-89-34-55.compute-1.amazonaws.com:20200/api/businesses';
        return await this.http.postApiAsync<any>(url,data);
    }

    async paymentBusinees(data: any)
    {
        let url = 'http://ec2-54-89-34-55.compute-1.amazonaws.com:20200/chargeMoneyViaStripe';
        return await this.http.getApiAsync<any>(url,data);
    }

    // checkExistBusiness(productCode: string): Promise<any>
    // {
    //     return new Promise( resolve => {
    //         // simulate a call to a web api with a setTimeout()
    //         setTimeout(() => {
    //             // pretent these are our products in our database
    //             var products: any[] = [{code: "P001", description: "Chai"}, {code: "P002", description: "Tofu"}, {code: "P003", description: "Pavlova"}];
    //             // this is the product code we want to check exists
    //             var productCodeToCheck: string = productCode;
    //             // check if the product code exists
    //             var foundProduct: any;
    //             products.forEach(function (product: any) {
    //                 if (productCodeToCheck === product.code) {
    //                     foundProduct = product;
    //                 }
    //             });
    //             resolve(foundProduct);
    //         }, 1000);
    //     });
    // }

    // checkExistEmail(productCode: string): Promise<any>
    // {
    //     return new Promise( resolve => {
    //         // simulate a call to a web api with a setTimeout()
    //         setTimeout(() => {
    //             // pretent these are our products in our database
    //             var products: any[] = [{code: "ducnv.viss@gmail.com", description: "Chai"}, {code: "ducnv.dev@hotmail.com", description: "Tofu"}, {code: "P003", description: "Pavlova"}];
    //             // this is the product code we want to check exists
    //             var productCodeToCheck: string = productCode;
    //             // check if the product code exists
    //             var foundProduct: any;
    //             products.forEach(function (product: any) {
    //                 if (productCodeToCheck === product.code) {
    //                     foundProduct = product;
    //                 }
    //             });
    //             resolve(foundProduct);
    //         }, 1000);
    //     });
    // }

    // checkExistPhone(productCode: string): Promise<any>
    // {
    //     return new Promise( resolve => {
    //         // simulate a call to a web api with a setTimeout()
    //         setTimeout(() => {
    //             // pretent these are our products in our database
    //             var products: any[] = [{code: "01693489360", description: "Chai"}, {code: "ducnv.dev@hotmail.com", description: "Tofu"}, {code: "P003", description: "Pavlova"}];
    //             // this is the product code we want to check exists
    //             var productCodeToCheck: string = productCode;
    //             // check if the product code exists
    //             var foundProduct: any;
    //             products.forEach(function (product: any) {
    //                 if (productCodeToCheck === product.code) {
    //                     foundProduct = product;
    //                 }
    //             });
    //             resolve(foundProduct);
    //         }, 1000);
    //     });
    // }

}
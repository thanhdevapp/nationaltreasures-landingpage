import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SignUpBusinessComponent } from './sign-up-business.component';
import { SignUpBusinessRoutes } from './sign-up-business.routing';
import { ReactiveFormsModule } from '@angular/forms';

@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,
    SignUpBusinessRoutes
  ],
  declarations: [SignUpBusinessComponent]
})
export class SignUpBusinessModule { }
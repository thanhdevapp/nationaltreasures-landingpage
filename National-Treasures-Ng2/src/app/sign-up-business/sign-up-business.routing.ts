import { Routes, RouterModule } from '@angular/router';
import { SignUpBusinessComponent } from './sign-up-business.component';

const routes: Routes = [
  { path: '', component: SignUpBusinessComponent } 
];

export const SignUpBusinessRoutes = RouterModule.forChild(routes);

import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { FormBuilder, FormGroup, Validators, AbstractControl, FormControl } from '@angular/forms';
import { Title } from '@angular/platform-browser';
import { ExtentionService, GlobalValidator, ValidationResult } from '../shared';
import { Observable } from 'rxjs/Observable';
import { SignUpBusinessService } from './sign-up-business.service';
import { element } from 'protractor';

declare var $;
declare var Stripe;

@Component({
  selector: 'app-sign-up-business',
  templateUrl: './sign-up-business.component.html',
  styleUrls: ['./sign-up-business.component.less'],
  encapsulation: ViewEncapsulation.None,
  providers: [ExtentionService, SignUpBusinessService]
})
export class SignUpBusinessComponent implements OnInit {

  paymentForm: FormGroup;
  myForm: FormGroup;
  isSubmit: boolean;
  paymentKey: string = 'pk_test_u8c6QtZM8gQ7tT0be2wvBy1F';
  token: string;

  constructor(private title: Title, private fb: FormBuilder, private ex: ExtentionService, private sv: SignUpBusinessService) {
    this.myForm = this.fb.group({
      name: ['', [Validators.required]],
      email: ['', [Validators.required, Validators.email]],
      phone: ['', [Validators.required, GlobalValidator.tel]],
      location: ['', [Validators.required]],
      logo: [''],
      location2: ['', [Validators.required]],
      cardNumber: ['', [Validators.pattern("[0-9]{0,16}")]],
      cardZipcode: ['', [Validators.pattern("[0-9]{0,5}")]],
      cardMonth: ['', [Validators.pattern("[0-9]{2}")]],
      cardYear: ['', [Validators.pattern("[0-9]{2}")]],
      cardcvc: ['', [Validators.pattern("[0-9]{3,4}")]],
      lat: ['0'],
      lng: ['0'],
      lat2: ['0'],
      lng2: ['0'],
      state: [''],
      country: [''],
      state2: [''],
      country2: [''],
    });

    this.paymentForm = this.fb.group({
      number: [''],
      zipcode: [''],
      month: [''],
      year: [''],
      cvc: [''],
    });
  }

  async ngOnInit() {
    $('.nano').nanoScroller();
    var outside = this;

    function readURL(input) {
      if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e: any) {
          $(input).parent().css({ 'background-image': 'url(' + e.target.result + ')', 'background-size': 'cover', 'border-style': 'solid' });
          $(input).parent().find('label').css({ 'opacity': '0' });
        }

        reader.readAsDataURL(input.files[0]);
      } else {
        $(input).parent().removeAttr('style');
        $(input).parent().find('label').css({ 'opacity': '1' });
      }
    }

    $(document).on('change', ".custom-file-input", function () {
      readURL(this);
    });

    $(document).on('click', '.closeImg', function () {
      var input = $(this).parent().find('input');
      $(input).val('');

      $(input).parent().removeAttr('style');
      $(input).parent().find('label').css({ 'opacity': '1' });
    });

    $("#location").geocomplete({
      //country: 'VN',
      details: ".detail-address",
      type: ['(regions)'],
      types: ["geocode", "establishment"],
    }).bind("geocode:result", function (event, result) {
      var location = result.geometry.location;
      outside.myForm.controls['lat'].setValue(location.lat());
      outside.myForm.controls['lng'].setValue(location.lng());
      let address = $(result.adr_address).text();
      outside.myForm.controls['location'].setValue(address);
      setTimeout(function () {
        let state = $('input[name="administrative_area_level_1"]', '.detail-address').val();
        let country = $('input[name="country"]', '.detail-address').val();
        outside.myForm.controls['state'].setValue(state);
        outside.myForm.controls['country'].setValue(country);
      }, 100);
    });

    $("#location2").geocomplete({
      //country: 'VN',
      details: ".detail-address2",
      type: ['(regions)'],
      types: ["geocode", "establishment"],
    }).bind("geocode:result", function (event, result) {
      var location = result.geometry.location;
      outside.myForm.controls['lat2'].setValue(location.lat());
      outside.myForm.controls['lng2'].setValue(location.lng());
      let address = $(result.adr_address).text();
      outside.myForm.controls['location2'].setValue(address);
      setTimeout(function () {
        let state = $('input[name="administrative_area_level_1"]', '.detail-address2').val();
        let country = $('input[name="country"]', '.detail-address').val();
        outside.myForm.controls['state2'].setValue(state);
        outside.myForm.controls['country2'].setValue(country);
      }, 100);
    });
  }

  async onSubmit() {
    console.log(this.myForm.value);
    this.ex.markFormGroupTouched(this.myForm);
    if (!this.myForm.valid) {
      return;
    }

    let outside = this;
    let formData = this.myForm.value;
    this.isSubmit = true;

    let logoUrl = "";
    if (formData.logo != '') {
      let logoFile = $('input[formControlName="logo"]')[0].files[0];
      var formLogo = new FormData();
      formLogo.append("file", logoFile, logoFile.name);
      formLogo.append("ownerId", formData.email);

      //begin animation upload
      let responseLogo = await this.sv.uploadLogo(formLogo);
      console.log(responseLogo);

      let fileData;
      if (responseLogo.success && responseLogo.data.length > 0) {
        fileData = responseLogo.data[0];
        logoUrl = fileData.downloadUrl;
      } else {
        //error
        location.href = '/sign-up-business-fail';
        return;
      }
      //end animation upload
    }

    let paramBesiness = {
      "name": formData.name,
      "email": formData.email,
      "tel": formData.phone,
      "address": formData.location,
      "location": {
        "lat": formData.lat,
        "lng": formData.lng
      },
      "logo": logoUrl,
      "nominateAddress": formData.location2,
      "nominateLocation": {
        "lat": formData.lat2,
        "lng": formData.lng2
      },
      "state": formData.state,
      "country": formData.country,
      "nominateState": formData.state2,
      "nominateCountry": formData.country2
    };

    let response = await this.sv.createBusinees(paramBesiness);
    let businessData;
    if (response.success) {
      businessData = response.data;
    } else {
      //error
      location.href = '/sign-up-business-fail';
      return;
    }

    if (this.token != null) {
      let paramPayment = {
        stripeToken: this.token,
        businessId: businessData.id
      }
      let responsePayment = await this.sv.paymentBusinees(paramPayment);
      let paymentData;
      if (responsePayment.success) {
        paymentData = responsePayment.data;
      } else {
        //error
        this.ex.showMsgSubmitFail(responsePayment.data.message);
        this.isSubmit = false;
        return;
      }
    }

    //submit form
    this.isSubmit = false;

    location.href = '/sign-up-business-success';
  }

  //function valid
  async checkStripe() {

    this.token = null;

    let cardNumber = this.myForm.controls['cardNumber'];
    let cardYear = this.myForm.controls['cardYear'];
    let cardMonth = this.myForm.controls['cardMonth'];
    let cardcvc = this.myForm.controls['cardcvc'];
    let cardZipcode = this.myForm.controls['cardZipcode'];

    // if (!cardNumber.valid || !cardYear.valid || !cardMonth.valid || !cardcvc.valid || !cardZipcode.valid) {
    //   return;
    // }

    this.paymentForm.controls['number'].setValue(cardNumber.value);
    this.paymentForm.controls['month'].setValue(cardMonth.value);
    this.paymentForm.controls['year'].setValue(cardYear.value);
    this.paymentForm.controls['cvc'].setValue(cardcvc.value);
    this.paymentForm.controls['zipcode'].setValue(cardZipcode.value);

    let outsite = this;
    Stripe.setPublishableKey(this.paymentKey);
    Stripe.card.createToken($('#payment-form'), function (status, response) {
      console.log(response);
      if (response.error) {
        outsite.token = null;
        //error
        switch (response.error.param) {
          case 'number':
            outsite.myForm.controls['cardNumber'].setErrors({ "paymentError": response.error.message });
            break;
          case 'exp_year':
            outsite.myForm.controls['cardYear'].setErrors({ "paymentError": 'Year is invalid' });
            break;
          case 'exp_month':
            outsite.myForm.controls['cardMonth'].setErrors({ "paymentError": 'Month is invalid' });
            break;
          case 'cvc':
            outsite.myForm.controls['cardcvc'].setErrors({ "paymentError": 'Cvc is invalid' });
            break;
          case 'address_zip':
            outsite.myForm.controls['cardZipcode'].setErrors({ "paymentError": response.error.message });
            break;

        }

      } else {
        //done
        outsite.token = response.id;
        outsite.myForm.controls['cardNumber'].setErrors(null);
        outsite.myForm.controls['cardYear'].setErrors(null);
        outsite.myForm.controls['cardMonth'].setErrors(null);
        outsite.myForm.controls['cardcvc'].setErrors(null);
        outsite.myForm.controls['cardZipcode'].setErrors(null);
      }
    });
  }

  async checkFile(e) {
    var element = e.currentTarget;
    let byteMax = 5 * 1024 * 1024;
    var name = element.attributes.formcontrolname.value;
    this.myForm.controls[name].markAsTouched();

    if (element.value != null && element.value != '') {
      this.myForm.controls[name].setErrors(null);
      if (element.files.length > 0) {
        if (element.files[0].size > byteMax) {
          this.myForm.controls[name].setErrors({ "maxsize": true });
          return;
        }
        if (element.files[0].type != "image/jpeg" && element.files[0].type != "image/png") {
          this.myForm.controls[name].setErrors({ "fomat": true });
          return;
        }
      }
    } else {
      if (name == 'logo') {
        this.myForm.controls[name].setErrors(null);
      } else {
        this.myForm.controls[name].setErrors({ "required": true });
      }
    }
  }



  // async validNameBusiness(control: FormControl) {
  //   if (control.hasError('required')) {
  //     return (null);
  //   }
  //   let sv = new SignUpBusinessService();
  //   const check = await sv.checkExistBusiness(control.value);
  //   if (check == null) {
  //     return (null);
  //   } else {
  //     return ({ businessExist: true });
  //   }
  // }

  // async validEmail(control: FormControl) {
  //   if (control.hasError('required') || control.hasError('email')) {
  //     return (null);
  //   }
  //   const sv = new SignUpBusinessService();
  //   const check = await sv.checkExistEmail(control.value);
  //   if (check == null) {
  //     return (null);
  //   } else {
  //     return ({ emailExist: true });
  //   }
  // }

  // async validPhone(control: FormControl) {
  //   if (control.hasError('required') || control.hasError('tel')) {
  //     return (null);
  //   }
  //   const sv = new SignUpBusinessService();
  //   const check = await sv.checkExistPhone(control.value);
  //   if (check == null) {
  //     return (null);
  //   } else {
  //     return ({ phoneExist: true });
  //   }
  // }
}

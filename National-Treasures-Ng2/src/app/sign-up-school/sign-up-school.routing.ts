import { Routes, RouterModule } from '@angular/router';
import { SignUpSchoolComponent } from './sign-up-school.component';

const routes: Routes = [
 { path: '', component: SignUpSchoolComponent } 
];

export const SignUpSchoolRoutes = RouterModule.forChild(routes);

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SignUpSchoolComponent } from './sign-up-school.component';
import { SignUpSchoolRoutes } from './sign-up-school.routing';
import { ReactiveFormsModule } from '@angular/forms';

@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,
    SignUpSchoolRoutes
  ],
  declarations: [SignUpSchoolComponent]
})
export class SignUpSchoolModule { }
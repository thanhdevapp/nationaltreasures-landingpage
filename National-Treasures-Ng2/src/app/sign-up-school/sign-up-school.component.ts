import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { FormBuilder, FormGroup, Validators, AbstractControl, FormControl, FormArray } from '@angular/forms';
import { Title } from '@angular/platform-browser';
import { ExtentionService, GlobalValidator, ValidationResult } from '../shared';
import { SignUpBusinessService } from '../sign-up-business/sign-up-business.service';
import { SignUpSchoolService } from './sign-up-school.service';

declare var $;
declare var Stripe;

@Component({
  selector: 'app-sign-up-school',
  templateUrl: './sign-up-school.component.html',
  styleUrls: ['./sign-up-school.component.less'],
  encapsulation: ViewEncapsulation.None,
  providers: [ExtentionService, SignUpSchoolService]
})
export class SignUpSchoolComponent implements OnInit {

  myForm: FormGroup;
  isSubmit: boolean;
  paymentKey: string = 'pk_test_u8c6QtZM8gQ7tT0be2wvBy1F';
  token: string;

  constructor(private title: Title, private fb: FormBuilder, private ex: ExtentionService, private sv: SignUpSchoolService) {
    this.myForm = this.fb.group({
      name: ['', [Validators.required]],
      email: ['', [Validators.required, Validators.email]],
      phone: ['', [Validators.required, GlobalValidator.tel]],
      location: ['', [Validators.required]],
      state: [''],
      country: [''],
      logo: ['', [Validators.required]],
      lat: ['0'],
      lng: ['0'],
      class1: this.fb.array([
        this.fb.group({
          file: ['']
        }),
        this.fb.group({
          file: ['']
        }),
        this.fb.group({
          file: ['']
        }),
        this.fb.group({
          file: ['']
        }),
        this.fb.group({
          file: ['']
        })
      ]),
      class2: this.fb.array([
        this.fb.group({
          file: ['']
        }),
        this.fb.group({
          file: ['']
        }),
        this.fb.group({
          file: ['']
        }),
        this.fb.group({
          file: ['']
        }),
        this.fb.group({
          file: ['']
        })
      ]),
      class3: this.fb.array([
        this.fb.group({
          file: ['']
        }),
        this.fb.group({
          file: ['']
        }),
        this.fb.group({
          file: ['']
        }),
        this.fb.group({
          file: ['']
        }),
        this.fb.group({
          file: ['']
        })
      ]),
      class4: this.fb.array([
        this.fb.group({
          file: ['']
        }),
        this.fb.group({
          file: ['']
        }),
        this.fb.group({
          file: ['']
        }),
        this.fb.group({
          file: ['']
        }),
        this.fb.group({
          file: ['']
        })
      ]),
      class5: this.fb.array([
        this.fb.group({
          file: ['']
        }),
        this.fb.group({
          file: ['']
        }),
        this.fb.group({
          file: ['']
        }),
        this.fb.group({
          file: ['']
        }),
        this.fb.group({
          file: ['']
        })
      ]),
      class6: this.fb.array([
        this.fb.group({
          file: ['']
        }),
        this.fb.group({
          file: ['']
        }),
        this.fb.group({
          file: ['']
        }),
        this.fb.group({
          file: ['']
        }),
        this.fb.group({
          file: ['']
        })
      ]),
      class7: this.fb.array([
        this.fb.group({
          file: ['']
        }),
        this.fb.group({
          file: ['']
        }),
        this.fb.group({
          file: ['']
        }),
        this.fb.group({
          file: ['']
        }),
        this.fb.group({
          file: ['']
        })
      ]),
    });
  }

  async ngOnInit() {
    $('.nano').nanoScroller();
    var outside = this;

    function readURL(input) {
      if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e: any) {
          $(input).parent().css({ 'background-image': 'url(' + e.target.result + ')', 'background-size': 'cover','border-style': 'solid' });
          $(input).parent().find('label').css({ 'opacity': '0' });
        }

        reader.readAsDataURL(input.files[0]);
      }else{
        $(input).parent().removeAttr('style');
        $(input).parent().find('label').css({ 'opacity': '1' });
      }
    }

    $(document).on('change', ".custom-file-input", function () {
      readURL(this);
    });

    $(document).on('click', '.closeImg', function () {
      var input = $(this).parent().find('input');
      $(input).val('');

      $(input).parent().removeAttr('style');
      $(input).parent().find('label').css({ 'opacity': '1' });
    });

    $(document).on('change', ".custom-file-input-square", function () {
      readURL(this);
    });

    $("#location").geocomplete({
      //country: 'VN',
      details: ".detail-address",
      type: ['(regions)'],
      types: ["geocode", "establishment"],
    }).bind("geocode:result", function (event, result) {
      console.log(result);
      var location = result.geometry.location;
      outside.myForm.controls['lat'].setValue(location.lat());
      outside.myForm.controls['lng'].setValue(location.lng());
      let address = $(result.adr_address).text();      
      outside.myForm.controls['location'].setValue(address);
      setTimeout(function() {
        let state = $('input[name="administrative_area_level_1"]','.detail-address').val();
        let country = $('input[name="country"]','.detail-address').val();
        outside.myForm.controls['state'].setValue(state);
        outside.myForm.controls['country'].setValue(country);
      }, 100);
      
    });
  }

  async onSubmit() {
    console.log(this.myForm.value);
    this.ex.markFormGroupTouched(this.myForm);
    if (!this.myForm.valid && this.token == null) {
      return;
    }

    let outside = this;
    let formData = this.myForm.value;
    this.isSubmit = true;

    let logoFile = $('input[formControlName="logo"]')[0].files[0];
    var formLogo = new FormData();
    formLogo.append("file", logoFile, logoFile.name);
    formLogo.append("ownerId", formData.email);

    //begin animation upload
    let responseLogo = await this.sv.uploadLogo(formLogo);
    console.log(responseLogo);
    
    let fileData;
    if (responseLogo.success && responseLogo.data.length > 0) {
      fileData = responseLogo.data[0];
    } else {
      //error
      location.href = '/sign-up-school-fail';
      return;
    }
    //end animation upload

    let paramSchool = {
      "name": formData.name,
      "address": formData.location,
      "state": formData.state,
      "country": formData.country,
      "location": {
        "lat": formData.lat,
        "lng": formData.lng
      },
      "email": formData.email,
      "tel": formData.phone,
      "logo": fileData.downloadUrl
    };
    let response = await this.sv.createSchool(paramSchool);
    console.log(response);
    
    let schoolData;
    if (response.success) {
      schoolData = response.data;
    } else {
      //error
      location.href = '/sign-up-school-fail';
      return;
    }

    //create class
    for (let i = 1; i <= 7; i++) {
      let paramYear = {
        "name": "Year " + i,
        "schoolId": schoolData.id
      }

      let responseClass = await this.sv.createClass(paramYear);
      console.log(responseClass);
      
      let classData;
      if (response.success) {
        classData = responseClass.data;
      } else {
        //error
        continue;
      }

      var lstFile = $('#class' + i).find('input');

      for (let j = 0; j < lstFile.length; j++) {
        if (lstFile[j].files.length == 0) {
          continue;
        }
        var file = lstFile[j].files[0];
        var formClass = new FormData();
        formClass.append("file", file, file.name);
        formClass.append("ownerId", schoolData.id);
        //begin animation upload
        let responseFile = await this.sv.uploadLogo(formClass);
        console.log(responseFile);
        
        let fileClassData;
        if (responseFile.success && responseFile.data.length > 0) {
          fileClassData = responseFile.data[0];
        } else {
          //error
          continue;
        }
        //end animation upload

        let paramTree = {
          "classId": classData.id,
          "photo": fileClassData.downloadUrl,
          "name": "Tree " + i + "-" + j
        };

        let responseTree = await this.sv.createTree(paramTree);
        console.log(responseTree);              
      }
    }
        
    //submit form
    this.isSubmit = false;

    location.href = '/sign-up-school-success';
  }

  //function valid
  async checkFile(e) {
    var element = e.currentTarget;
    let byteMax = 5 * 1024 * 1024;
    var name = element.attributes.formcontrolname.value;
    this.myForm.controls[name].markAsTouched();
    //this.myForm.controls['check'+name].setValue(element.value);

    if (element.value != null && element.value != '') {
      this.myForm.controls[name].setErrors(null);
      if (element.files.length > 0) {
        if (element.files[0].size > byteMax) {
          this.myForm.controls[name].setErrors({ "maxsize": true });
          return;
        }
        if (element.files[0].type != "image/jpeg" && element.files[0].type != "image/png") {
          this.myForm.controls[name].setErrors({ "fomat": true });
          return;
        }
      }
    } else {
      this.myForm.controls[name].setErrors({ "required": true });
    }
  }

  // async validNameBusiness(control: FormControl) {
  //   if (control.hasError('required')) {
  //     return (null);
  //   }
  //   let sv = new SignUpBusinessService();
  //   let check = await sv.checkExistBusiness(control.value);
  //   if (check == null) {
  //     return (null);
  //   } else {
  //     return ({ businessExist: true });
  //   }
  // }

  // async validEmail(control: FormControl) {
  //   if (control.hasError('required') || control.hasError('email')) {
  //     return (null);
  //   }
  //   let sv = new SignUpBusinessService();
  //   let check = await sv.checkExistEmail(control.value);
  //   if (check == null) {
  //     return (null);
  //   } else {
  //     return ({ emailExist: true });
  //   }
  // }

  // async validPhone(control: FormControl) {
  //   if (control.hasError('required') || control.hasError('tel')) {
  //     return (null);
  //   }
  //   let sv = new SignUpBusinessService();
  //   let check = await sv.checkExistPhone(control.value);
  //   if (check == null) {
  //     return (null);
  //   } else {
  //     return ({ phoneExist: true });
  //   }
  // }

}

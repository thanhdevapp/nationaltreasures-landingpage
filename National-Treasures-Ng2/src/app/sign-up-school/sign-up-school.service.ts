import { Injectable } from '@angular/core';
import { HttpService } from '../shared';

@Injectable()
export class SignUpSchoolService {

    constructor(private http: HttpService) { }

    async createSchool(data: any)
    {
        let url = 'http://ec2-54-89-34-55.compute-1.amazonaws.com:20200/api/schools';
        return await this.http.postApiAsync(url,data);
    }

    async createClass(data: any)
    {
        let url = 'http://ec2-54-89-34-55.compute-1.amazonaws.com:20200/api/classes';
        return await this.http.postApiAsync(url,data);
    }

    async createTree(data: any)
    {
        let url = 'http://ec2-54-89-34-55.compute-1.amazonaws.com:20200/api/trees';
        return await this.http.postApiAsync(url,data);
    }

    async uploadLogo(form: any)
    {
        let url = 'http://ec2-54-89-34-55.compute-1.amazonaws.com:20200/api/storages/logos/upload';
        return await this.http.uploadApiAsync<any[]>(url,form);
    }

}
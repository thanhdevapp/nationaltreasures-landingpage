import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Title } from '@angular/platform-browser';

declare var $;

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.less'],
  encapsulation: ViewEncapsulation.None 
})
export class HomeComponent implements OnInit {
  myForm: FormGroup;
  isSubmit: boolean;

  constructor(private title: Title, private fb: FormBuilder) {
    this.myForm = this.fb.group({
      name: ['', [Validators.required]],
      email: [''],
      phone: [''],
      comment: ['']
    });
  }

  ngOnInit() {
    $('.nano').nanoScroller();
    var owl = $('.owl-carousel');
    owl.owlCarousel({
        animateIn: 'slideInRight',
        animateOut: 'fadeOut',
        margin: 17,
        loop: true,
        items: 1,
        autoWith: true,
        nav: false,
        dots: true,
        autoplay: true,
        autoplayTimeout: 10000
      });
      // Listen to owl events:
      owl.on('changed.owl.carousel', function (event) {
        var page = event.page.index + 1;     // Position of the current page
      });

    $(document).on('click', '.ctrl-slide .btn-prev', function () {
      owl.trigger('prev.owl.carousel');
    });

    $(document).on('click', '.ctrl-slide .btn-next', function () {
      owl.trigger('next.owl.carousel');
    });

  }

}

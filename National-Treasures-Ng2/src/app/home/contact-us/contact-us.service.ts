import { Injectable } from '@angular/core';
import { HttpService } from '../../shared/services/http.service';

@Injectable()
export class ContactUsService {

constructor(private http: HttpService) { }

    async sentData(body: any, showErrorDialog: boolean = false) {
        let url = 'http://ec2-54-89-34-55.compute-1.amazonaws.com:20200/api/pushNotificationEmails/contactUs';
        return await this.http.postApiAsync(url,body);
    }

}

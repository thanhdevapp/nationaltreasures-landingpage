import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { GlobalValidator } from '../../shared/class/GlobalValidator';
import { ContactUsService } from './contact-us.service';
import { ExtentionService } from '../../shared';

@Component({
  selector: 'app-contact-us',
  templateUrl: './contact-us.component.html',
  styleUrls: ['./contact-us.component.less'],
  providers: [ContactUsService, ExtentionService]
})
export class ContactUsComponent implements OnInit {
  myForm: FormGroup;
  isSubmit: boolean;
  constructor(private fb: FormBuilder, private sv: ContactUsService, private ex: ExtentionService) {
    this.myForm = this.fb.group({
      name: ['', [Validators.required]],
      email: ['', [Validators.required, Validators.email]],
      phone: ['', [Validators.required, GlobalValidator.tel]],
      comment: ['', [Validators.required]],
    });
  }

  async ngOnInit() {

  }

  async onSubmit() {
    this.ex.markFormGroupTouched(this.myForm);
    if (!this.myForm.valid) {
      return;
    }
    this.isSubmit = true;
    let formdata = this.myForm.value;
    let param = {
      "email": formdata.email,
      "content": formdata.comment,
      "name": formdata.name,
      "tel": formdata.phone
    }
    let response = await this.sv.sentData(param);
    if (response.success) {
      this.ex.showMsgSuccessfully('Your comment has been submitted successfully, please check your email.')
    } else {
      //error
      this.ex.showMsgSubmitFail('There was an error trying to submit your enquiries. Please try again later.')
    }
    //submit form
    this.myForm.reset();
    this.isSubmit = false;
  }
}

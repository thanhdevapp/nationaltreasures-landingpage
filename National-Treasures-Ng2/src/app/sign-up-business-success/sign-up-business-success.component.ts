import { Component, OnInit, ViewEncapsulation } from '@angular/core';

declare var $;

@Component({
  selector: 'app-sign-up-business-success',
  templateUrl: './sign-up-business-success.component.html',
  styleUrls: ['./sign-up-business-success.component.less'],
  encapsulation: ViewEncapsulation.None,
})
export class SignUpBusinessSuccessComponent implements OnInit {

  constructor() { }

  ngOnInit() {
    $('.nano').nanoScroller();
  }

}

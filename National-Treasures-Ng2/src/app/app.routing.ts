import { SignUpSchoolSuccessComponent } from './sign-up-school-success/sign-up-school-success.component';
import { Routes, RouterModule } from '@angular/router';
import { SignUpSchoolFailComponent } from './sign-up-school-fail/sign-up-school-fail.component';
import { SignUpBusinessSuccessComponent } from './sign-up-business-success/sign-up-business-success.component';
import { SignUpBusinessFailComponent } from './sign-up-business-fail/sign-up-business-fail.component';

const routes: Routes = [
  { path: '', loadChildren: './home/home.module#HomeModule' },  
  { path: 'sign-up', loadChildren: './sign-up-index/sign-up-index.module#SignUpIndexModule' },  
  { path: 'sign-up-business', loadChildren: './sign-up-business/sign-up-business.module#SignUpBusinessModule' },  
  { path: 'sign-up-business-success', component: SignUpBusinessSuccessComponent },  
  { path: 'sign-up-business-fail', component: SignUpBusinessFailComponent },  
  { path: 'sign-up-school', loadChildren: './sign-up-school/sign-up-school.module#SignUpSchoolModule' }, 
  { path: 'sign-up-school-success', component: SignUpSchoolSuccessComponent },  
  { path: 'sign-up-school-fail', component: SignUpSchoolFailComponent },  
  { path: 'app', loadChildren: './landing-app/landing-app.module#LandingAppModule' },  
  //{ path: '**', redirectTo: '', pathMatch: 'full' }
];

export const AppRoutes = RouterModule.forRoot(routes);

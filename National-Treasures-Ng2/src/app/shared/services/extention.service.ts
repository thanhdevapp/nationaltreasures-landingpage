import { Injectable } from '@angular/core';
import { FormControl, Validators, FormArray, FormGroup, FormBuilder } from '@angular/forms';

declare var $: any;

@Injectable()
export class ExtentionService {

    constructor(private fb: FormBuilder) { }

    public getUrlParameter(sParam, search: string = null) {
        if (search == null) { search = window.location.search }
        search = search.replace('?', '');
        var sPageURL = decodeURIComponent(search),
            sURLVariables = sPageURL.split('&'),
            sParameterName,
            i;

        for (i = 0; i < sURLVariables.length; i++) {
            sParameterName = sURLVariables[i].split('=');

            if (sParameterName[0] === sParam) {
                return sParameterName[1] === undefined ? true : sParameterName[1];
            }
        }
    };

    public getFormGroup(obj: any, valid: boolean = true): FormGroup {
        let formGroup: FormGroup = this.fb.group({});
        obj.forEach(x => {
            if (!x.isObject) {
                let formControl = this.getFormControll(x, valid);
                formGroup.addControl(x.name, formControl);
            } else {
                if (x.isArray) {
                    let formArray: FormArray = this.getFormArray(x);
                    formGroup.addControl(x.name, formArray)
                } else {
                    let formGroupChild: FormGroup = this.getFormGroup(x);
                    formGroup.addControl(x.name, formGroupChild)
                }
            }
        });
        return formGroup;
    }

    public confirmDialog(title: string = '', content: string = ''): Promise<any> {
        return new Promise(resolve => {
            $.confirm({
                type: 'red',
                title: title,
                content: content,
                buttons: {
                    ok: {
                        text: 'Ok',
                        keys: ['enter'],
                        action: function () {
                            resolve(true);
                        }
                    },
                    cancel: {
                        text: 'cancel',
                        keys: ['esc'],
                        action: function () {
                            resolve(false);
                        }
                    }
                }
            });
        });
    }

    public alertDialog(title: string = '', content: string = '') {
        $.alert({
            type: 'red',
            title: title,
            content: content,
            buttons: {
                ok: {
                    text: 'Ok',
                    keys: ['enter'],
                    action: function () { }
                }
            }
        })
    }

    public markFormGroupTouched(formGroup: FormGroup) {
        (<any>Object).values(formGroup.controls).forEach(control => {
            control.markAsTouched();

            if (control.controls) {
                control.controls.forEach(c => this.markFormGroupTouched(c));
            }
        });
    }

    //set icon button submit success
    public showMsgSuccessfully(msg: string) {
        $('.msg').text(msg);
        $('.error-fixed-bottom').css({ 'opacity': 1, 'transform': 'scale(1)' });
        $('.error-fixed-bottom .icon').attr('src', '/assets/icons/ic-submit-successful.svg');
    }

    public showMsgSubmitFail(msg: string) {
        $('.msg').text(msg);
        $('.error-fixed-bottom').css({ 'opacity': 1, 'transform': 'scale(1)' });
        $('.error-fixed-bottom .icon').attr('src', 'icons/ic-submitfail.svg');
    }

    //-----private-----
    private getFormControll(obj: any, val: boolean = true): FormControl {
        let valid = [];
        if (val) {
            obj.validate.forEach(v => {
                switch (v.name) {
                    case 'Required':
                        valid.push(Validators.required);
                        break;
                    case 'Compare':
                        //valid.push(Validators.required);
                        break;
                    case 'StringLength':
                        //valid.push(Validators.required);
                        break;
                    case 'MinLength':
                        valid.push(Validators.min(v.value.minLength));
                        break;
                    case 'MaxLength':
                        valid.push(Validators.min(v.value.maxLength));
                        break;
                    case 'EmailAddress':
                        valid.push(Validators.email);
                        break;
                    case 'Phone':
                        //valid.push(Validators.required);
                        break;
                    case 'RegularExpression':
                        //valid.push(Validators.required);
                        break;
                }
            });
        }
        let ctrl: FormControl = this.fb.control(obj.value, valid);
        return ctrl;
    }

    private getFormArray(obj: any): FormArray {
        let lstFormGroup: FormGroup[] = [];
        obj.value.forEach(x => {
            let formGroup: FormGroup = this.getFormGroup(x);
            lstFormGroup.push(formGroup);
        });
        let formArray: FormArray = this.fb.array(lstFormGroup);
        return formArray;
    }
}
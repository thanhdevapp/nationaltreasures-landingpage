import {HttpModule} from '@angular/http';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { AppRoutes } from './app.routing';
import { HttpService, ExtentionService } from './shared/services';
import { SignUpSchoolSuccessComponent } from './sign-up-school-success/sign-up-school-success.component';
import { SignUpSchoolFailComponent } from './sign-up-school-fail/sign-up-school-fail.component';
import { SignUpBusinessSuccessComponent } from './sign-up-business-success/sign-up-business-success.component';
import { SignUpBusinessFailComponent } from './sign-up-business-fail/sign-up-business-fail.component';

@NgModule({
  declarations: [
    AppComponent,
    SignUpSchoolSuccessComponent,
    SignUpSchoolFailComponent,
    SignUpBusinessSuccessComponent,
    SignUpBusinessFailComponent
],
  imports: [
    BrowserModule,
    HttpModule,
    AppRoutes
  ],
  providers: [
    HttpService,
    ExtentionService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }

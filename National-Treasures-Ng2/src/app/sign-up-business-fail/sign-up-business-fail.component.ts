import { Component, OnInit, ViewEncapsulation } from '@angular/core';

declare var $;

@Component({
  selector: 'app-sign-up-business-fail',
  templateUrl: './sign-up-business-fail.component.html',
  styleUrls: ['../sign-up-business-success/sign-up-business-success.component.less'],
  encapsulation: ViewEncapsulation.None,
})
export class SignUpBusinessFailComponent implements OnInit {

  constructor() { }

  ngOnInit() {
    $('.nano').nanoScroller();
  }

}
